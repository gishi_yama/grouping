package org.wicket_sapporo.grouping.page;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.bootstrap.Bootstrap;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.model.util.ListModel;
import org.wicket_sapporo.grouping.bean.GroupUser;

/**
 * 投稿された学籍番号をグループに分けてテキストファイルに出力するページ.
 */
public class HomePage extends WebPage {
	private static final long serialVersionUID = 1L;

	// 配属させるグループ名の一覧
	public static final String[] groups = { "1番目", "2番目", "3番目", "4番目" };
	// 1グループあたりの人数
	public static final int MAX_MEMBER_PER_GROUP = 1;
	// ファイル名
	public static final String filename = "output/" +System.currentTimeMillis() + ".txt";

	// コンポーネント用のモデル
	private IModel<String> userNameModel;
	private IModel<String> messageModel;
	private IModel<List<GroupUser>> groupUserListModel;

	// グループあたりの所属人数
	private Map<String, Integer> groupLimit;

	//乱数オブジェクト
	private Random random;

	/**
	 * Construct.
	 */
	public HomePage() {
		messageModel = Model.of("");
		userNameModel = Model.of("");
		groupUserListModel = new ListModel<>(new ArrayList<GroupUser>(groups.length * MAX_MEMBER_PER_GROUP));
		groupLimit = new HashMap<String, Integer>();
		random = new Random();

		for (String group : groups) {
			groupLimit.put(group, MAX_MEMBER_PER_GROUP);
		}


		add(new Label("message", messageModel));

		Form<Void> form = new Form<Void>("form") {
			private static final long serialVersionUID = 2309021966798947353L;

			@Override
			protected void onSubmit() {
				super.onSubmit();
				GroupUser groupUser = new GroupUser();
				for (GroupUser gu : groupUserListModel.getObject()) {
					if (StringUtils.equals(gu.getUserName(), userNameModel.getObject())) {
						error(getString("ERROR_DUPLICATE_MESSAGE"));
						return;
					}
				}
				int limit = 0;
				for (String group : groups) {
					limit += groupLimit.get(group);
				}
				if (limit == 0) {
					error(getString("ERROR_LIMITATION_MESSAGE"));
					return;
				}

				while (true) {
					int no = random.nextInt(groups.length);
					limit = groupLimit.get(groups[no]);
					if (limit > 0) {
						groupLimit.put(groups[no], limit - 1);
						groupUser.setGroupName(groups[no]);
						groupUser.setUserName(userNameModel.getObject());
						break;
					}
				}
				groupUserListModel.getObject().add(groupUser);
				messageModel.setObject(new StringResourceModel("SUCCESS_MESSAGE", this, Model.of(groupUser)).getObject());

				Path newFilePath = FileSystems.getDefault().getPath(filename);
				try (BufferedWriter bw =
						Files.newBufferedWriter(newFilePath, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
								StandardOpenOption.APPEND)) {
					bw.write(messageModel.getObject() + "\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
				userNameModel.setObject("");
			}
		};
		add(form);

		form.add(new FeedbackPanel("feedback"));
		form.add(new RequiredTextField<>("userName", userNameModel));

		add(new PropertyListView<GroupUser>("groupUsers", groupUserListModel) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<GroupUser> i) {
				i.add(new Label("userName"));
				i.add(new Label("groupName"));
			}
		});
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);
		Bootstrap.renderHeadPlain(response);
		Bootstrap.renderHeadResponsive(response);
	}
}
