/**
 *
 */
package org.wicket_sapporo.grouping.bean;

import java.io.Serializable;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class GroupUser implements Serializable {

	private static final long serialVersionUID = -6296525900157322622L;
	private String userName;
	private String groupName;

	public GroupUser() {
		this.userName = "";
		this.groupName = "";
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *          the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName
	 *          the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
}
