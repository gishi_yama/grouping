package org.wicket_sapporo;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.List;

import org.apache.wicket.Component;
import org.apache.wicket.util.tester.FormTester;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Before;
import org.junit.Test;
import org.wicket_sapporo.grouping.WicketApplication;
import org.wicket_sapporo.grouping.bean.GroupUser;
import org.wicket_sapporo.grouping.page.HomePage;

/**
 * Simple test using the WicketTester
 */
public class TestHomePage {
	private WicketTester tester;

	@Before
	public void setUp() {
		tester = new WicketTester(new WicketApplication());
	}

	@Test
	public void ページが表示される() {
		tester.startPage(HomePage.class);
		tester.assertRenderedPage(HomePage.class);
	}

	@Test
	public void 値を入れずに送信ボタンを押すとエラーが表示される() {
		tester.startPage(HomePage.class);
		FormTester fTester = tester.newFormTester("form");
		fTester.submit();
		tester.assertFeedback("form:feedback", "'学籍番号' 欄 は必須です。");
	}

	@Test
	public void 適切な値を入れて送信ボタンを押すとメッセージと結果リストが更新される() {
		tester.startPage(HomePage.class);
		FormTester fTester = tester.newFormTester("form");
		fTester.setValue("userName", "a");
		fTester.submit();
		Component groupUsers = tester.getComponentFromLastRenderedPage("groupUsers");
		@SuppressWarnings("unchecked")
		List<GroupUser> obj = (List<GroupUser>) groupUsers.getDefaultModelObject();
		assertThat(obj.size(), is(1));
		tester.assertLabel("groupUsers:0:userName", obj.get(0).getUserName());
		tester.assertLabel("groupUsers:0:groupName", obj.get(0).getGroupName());
		tester.assertLabel("message", obj.get(0).getUserName() + " は " + obj.get(0).getGroupName() + " です。");
	}

	@Test
	public void 入力値が重複しているとエラーが生じる() {
		tester.startPage(HomePage.class);
		FormTester fTester = tester.newFormTester("form");
		fTester.setValue("userName", "a");
		fTester.submit();
		fTester = tester.newFormTester("form");
		fTester.setValue("userName", "a");
		fTester.submit();

		tester.assertFeedback("form:feedback", "このユーザはすでに登録済みです。");
		Component groupUsers = tester.getComponentFromLastRenderedPage("groupUsers");
		@SuppressWarnings("unchecked")
		List<GroupUser> obj = (List<GroupUser>) groupUsers.getDefaultModelObject();
		assertThat(obj.size(), is(1));
		tester.assertLabel("groupUsers:0:userName", obj.get(0).getUserName());
		tester.assertLabel("groupUsers:0:groupName", obj.get(0).getGroupName());
		tester.assertLabel("message", obj.get(0).getUserName() + " は " + obj.get(0).getGroupName() + " です。");
	}

	@Test
	public void 回数制限を超えて投稿するとエラーが生じる() {
		tester.startPage(HomePage.class);
		int max = HomePage.groups.length * HomePage.MAX_MEMBER_PER_GROUP;
		FormTester fTester;
		for(int i = 0 ; i <= max; i++) {
			fTester = tester.newFormTester("form");
			fTester.setValue("userName", "a"+i);
			fTester.submit();
		}

		tester.assertFeedback("form:feedback", "全てのグループが最大人数に達しています。");
		Component groupUsers = tester.getComponentFromLastRenderedPage("groupUsers");
		@SuppressWarnings("unchecked")
		List<GroupUser> obj = (List<GroupUser>) groupUsers.getDefaultModelObject();
		assertThat(obj.size(), is(max));
		tester.assertLabel("groupUsers:0:userName", obj.get(0).getUserName());
		tester.assertLabel("groupUsers:0:groupName", obj.get(0).getGroupName());
		tester.assertLabel("message", obj.get(max-1).getUserName() + " は " + obj.get(max-1).getGroupName() + " です。");
	}

}
